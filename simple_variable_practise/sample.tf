# sample .tf file to launch an instance using varibales

resource "aws_instance" "variable_instance" {
    ami = var.image_id
    instance_type = var.machine_type
    key_name = "mumbai-key"
    vpc_security_group_ids = ["sg-0aec900e0d1cdd732"]
    tags = {
        Name = "Variable-Server"
        env = "dev"
    }
}


# Variable creating 

variable "image_id" {
    default = "ami-03f4878755434977f"
}

variable "machine_type" {
    default = "t2.micro"
}

variable "vpc_id" {
    default = "vpc-07a408e8c728fe382"
}
# Simple .tf file to launch an instance

provider "aws" {
    region = "ap-south-1"
}

resource "aws_instance" "another" {
    ami = "ami-03f4878755434977f"
    instance_type = "t2.micro"
    key_name = "mumbai-key"
    vpc_security_group_ids = ["sg-0aec900e0d1cdd732"]
    tags = {
        Name = "another"
        env = "dev"
    }
}
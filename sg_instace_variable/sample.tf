provider "aws" {
    region = "ap-south-1"
}

# sample .tf for creating security group and instance using variable and providing that security group to instance
resource "aws_security_group" "my_sec" {
    name = "my-sec-group"
    description = "allow SSH & HTTP"
    vpc_id = var.vpc_id
    ingress {
        protocol = "TCP"
        from_port = 22
        to_port = 22
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        protocol = "TCP"
        from_port = 80
        to_port = 80
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        protocol = "-1"  
        from_port = 0
        to_port = 0
        cidr_blocks = ["0.0.0.0/0"]
    }

  
}

#creating instance using values of variables
resource "aws_instance" "aws_instance_resource1" {
    ami = var.image_id
    instance_type = var.machine_type
    key_name = "mumbai-key"
    vpc_security_group_ids = ["sg-0aec900e0d1cdd732", aws_security_group.my_sec.id ]
    tags = {
        Name = "sg-server1"
        env = "dev"
    }
}
resource "aws_instance" "aws_instance_resource2" {
  ami = var.image_id
  instance_type = var.machine_type
  key_name = "mumbai-key"
  vpc_security_group_ids = ["sg-0aec900e0d1cdd732"]
  tags = {
    Name = "sg-server-2"
    env = "test"
  }
}

# variable creating
variable "vpc_id" {
    default = "vpc-07a408e8c728fe382"
  
}
variable "image_id" {
    default = "ami-03f4878755434977f"
  
}
variable "machine_type" {
  default = "t3.micro"
}

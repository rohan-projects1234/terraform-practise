provider "aws" {
  region = "ap-south-1"
}

# Creating VPC
resource "aws_vpc" "main_vpc" {
  cidr_block = var.vpc_cidr
  tags = {
    Name = "${var.project}-VPC"
    env = var.env
  }
}

# Creating Private Subnet
resource "aws_subnet" "private_subnet" {
  vpc_id = aws_vpc.main_vpc.id
  cidr_block = var.private_subnet_cidr
  tags = {
    Name = "${var.project}-private-subnet"
    env = "var.env"
  }
}

# Creating Public Subnet
resource "aws_subnet" "public_subnet" {
  vpc_id = aws_vpc.main_vpc.id
  cidr_block = var.public_subnet_cidr  
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.project}-public-subnet"
    env = var.env
  }
}

# Creating Internet getway
resource "aws_internet_gateway" "main_igw" {
  vpc_id = aws_vpc.main_vpc.id
  tags = {
    Name = "${var.project}-igw"
    env = var.env
  }
}

# routing in default route
resource "aws_route" "igw_routing" {
  route_table_id = aws_vpc.main_vpc.default_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.main_igw.id
}

# Creating security group
resource "aws_security_group" "rohan-sg" {
  name = "${var.project}-sg"
  description = "Allow SSH and HTTP"
  vpc_id = aws_vpc.main_vpc.id

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    protocol = "TCP"
    from_port = 22
    to_port = 22
  }
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    protocol = "TCP"
    from_port = 80
    to_port = 80
  }
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    protocol = "-1"
    from_port = 0
    to_port = 0
  }
  
  depends_on = [ aws_internet_gateway.main_igw ]
}

# Creating instance from private subnet
resource "aws_instance" "private_instance" {
  ami = var.image_id
  instance_type = var.machine_type
  key_name = var.key_pair
  vpc_security_group_ids = [aws_security_group.rohan-sg.id]
  tags = {
    Name = "${var.project}-private-server"
    env = var.env
  }
  subnet_id = aws_subnet.private_subnet.id
}

# Creating instance from public subnet
resource "aws_instance" "public_instance" {
  ami = var.image_id
  instance_type = var.machine_type
  key_name = var.key_pair
  vpc_security_group_ids = [aws_security_group.rohan-sg.id]
  tags = {
    Name = "${var.project}-public-server"
    env = var.env
  }
  subnet_id = aws_subnet.public_subnet.id

}
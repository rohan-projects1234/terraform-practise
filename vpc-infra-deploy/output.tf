output "vpc-id" {
  value = "Project VPC id :- ${aws_vpc.main_vpc.id}"
}

output "private-subnet" {
  value = "Private subnet id :- ${aws_subnet.private_subnet.id}"
}

output "public-subnet" {
  value = "Public subnet id :- ${aws_subnet.public_subnet.id}"
}

output "internet-getway-id" {
  value = "Internet getway id :- ${aws_internet_gateway.main_igw.id}"
}

output "security_group_id_name" {
  value = "Securty group id :- ${aws_security_group.rohan-sg.id} & Security group name :- ${aws_security_group.rohan-sg.name}"
}

output "private-instance-state" {
  value = "${var.project}-private-server state :- ${aws_instance.private_instance.instance_state}"
}

output "private-instance-pr-ip" {
  value = "Private ip of ${var.project}-private-server :- ${aws_instance.private_instance.private_ip}"
}

output "public-instance-state" {
  value = "${var.project}-public-server state :- ${aws_instance.public_instance.instance_state}"
}

output "public-instance-pr-ip" {
  value = "Private ip of ${var.project}-public-server :- ${aws_instance.public_instance.private_ip}"
}

output "public-instance-pb-ip" {
  value = "public ip of ${var.project}-public-server :- ${aws_instance.public_instance.public_ip}"
}
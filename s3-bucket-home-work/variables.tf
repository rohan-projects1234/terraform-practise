variable "user_name" {
  default = "s3_user"
}

variable "bucket_name" {
  default = "shark-bucket3112"
}

variable "bucket_tag_name" {
  default = "Rony-bucket"
}

variable "env" {
  default = "dev"
}

variable "action_effect" {
  default = "Allow"
}

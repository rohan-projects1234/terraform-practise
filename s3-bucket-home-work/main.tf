provider "aws" {
  region = "ap-south-1"
}

# Creating IAM User
resource "aws_iam_user" "iam_user" {
  name = var.user_name
}

#Creating s3 bucket
resource "aws_s3_bucket" "s3_bucket_create" {
  bucket = var.bucket_name

  tags = {
    Name        = var.bucket_tag_name
    Environment = var.env
  }
}


# Creating IAM policy to access that bucket only
resource "aws_iam_policy" "iam_s3_policy" {
  name        = "${var.bucket_name}-Policy"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "s3:GetObject",
          "s3:ListBucket"
        ]
        Effect   = "Allow"
        Resource = aws_s3_bucket.s3_bucket_create.arn
      },
    ]
  })
}

# creating s3 bucket policy (resource policy) to acces above bucket only
/*resource "aws_s3_bucket_policy" "allow_access_from_another_account" {
  bucket = aws_s3_bucket.s3_bucket_create.id
  policy = data.aws_iam_policy_document.allow_access_from_another_account.json
}

data "aws_iam_policy_document" "allow_access_from_another_account" {
  statement {
    principals {
      type        = "AWS"
      identifiers = [aws_iam_user.iam_user.arn]
    }

    actions = [
      "s3:GetObject",
      "s3:ListBucket"
    ]

    resources = [
      aws_s3_bucket.s3_bucket_create.arn,
      "${aws_s3_bucket.s3_bucket_create.arn}/*",
    ]
  }
}*/

# Attach policy to user
resource "aws_iam_user_policy_attachment" "policy-attach" {
  user       = aws_iam_user.iam_user.name
  policy_arn = aws_iam_policy.iam_s3_policy.arn
}
